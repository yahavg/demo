//package daytwo.demo.poc.service;
//
//
//import com.datastax.driver.core.exceptions.DriverException;
//import com.datastax.driver.core.querybuilder.Delete;
//import com.datastax.driver.core.querybuilder.QueryBuilder;
//import com.datastax.driver.core.querybuilder.Select;
//import com.datastax.driver.core.querybuilder.Update;
//import daytwo.demo.poc.repository.GreetRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.cassandra.core.CassandraOperations;
//
//import java.util.ArrayList;
//
//
//public class GreetService {
//    @Autowired
//    GreetRepository repository;
//
//    @Autowired
//    private CassandraOperations cassandraOperations;
//
//    public GreetService() {
//
//
//
//    }
//
//    public void save()
//    {
//        String cql = "insert into person (id, name, age) values ('123123123', 'Alison', 39)";
//        cassandraOperations.execute(cql);
//
//        ///
//        String cqlIngest = "insert into person (id, name, age) values (?, ?, ?)";
//        List<Object> person1 = new ArrayList<Object>();
//        person1.add("10000");
//        person1.add("David");
//        person1.add(40);
//
//        List<Object> person2 = new ArrayList<Object>();
//        person2.add("10001");
//        person2.add("Roger");
//        person2.add(65);
//
//        List<List<?>> people = new ArrayList<List<?>>();
//        people.add(person1);
//        people.add(person2);
//
//        cassandraOperations.ingest(cqlIngest, people);
//    }
//
//    public void update() {
//        cassandraOperations.update(new Person("123123123", "Alison", 35));
//        ///
//        Update update = QueryBuilder.update("person");
//        update.setConsistencyLevel(ConsistencyLevel.ONE);
//        update.with(QueryBuilder.set("age", 35));
//        update.where(QueryBuilder.eq("id", "123123123"));
//
//        cassandraOperations.execute(update);
//        ///
//        String cql = "update person set age = 35 where id = '123123123'";
//        cassandraOperations.execute(cql);
//    }
//
//    public void delete() {
//        cassandraOperations.delete(new Person("123123123", null, 0));
//        ///
//        Delete delete = QueryBuilder.delete().from("person");
//        delete.where(QueryBuilder.eq("id", "123123123"));
//        cassandraOperations.execute(delete);
//        ///
//        String cql = "delete from person where id = '123123123'";
//        cassandraOperations.execute(cql);
//    }
//
//    public void select(){
//        String cqlAll = "select * from person";
//        List<Person> results = cassandraOperations.select(cqlAll, Person.class);
//        for (Person p : results) {
//            LOG.info(String.format("Found People with Name [%s] for id [%s]", p.getName(), p.getId()));
//        }
//
//        ///Query a table for a single row and map the result to a POJO.
//        String cqlOne = "select * from person where id = '123123123'";
//        Person p = cassandraOperations.selectOne(cqlOne, Person.class);
//        LOG.info(String.format("Found Person with Name [%s] for id [%s]", p.getName(), p.getId()));
//
//        ///Query a table using the QueryBuilder.Select object that is part of the DataStax Java Driver.
//        Select select = QueryBuilder.select().from("person");
//        select.where(QueryBuilder.eq("id", "123123123"));
//        Person p = cassandraOperations.selectOne(select, Person.class);
//        LOG.info(String.format("Found Person with Name [%s] for id [%s]", p.getName(), p.getId()));
//
//        ///Then there is always the old fashioned way. You can write your own CQL statements, and there are several callback handlers for mapping the results. The example uses the RowMapper interface.
//        String cqlAll = "select * from person";
//        List<Person> results = cassandraOperations.query(cqlAll, new RowMapper<Person>() {
//
//            public Person mapRow(Row row, int rowNum) throws DriverException {
//                Person p = new Person(row.getString("id"), row.getString("name"), row.getInt("age"));
//                return p;
//            }
//        });
//
//        for (Person p : results) {
//            LOG.info(String.format("Found People with Name [%s] for id [%s]", p.getName(), p.getId()));
//        }
//
//    }
//
//}
