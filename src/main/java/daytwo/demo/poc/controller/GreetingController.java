package daytwo.demo.poc.controller;

import com.datastax.driver.core.utils.UUIDs;
import daytwo.demo.poc.domain.Greeting;
import daytwo.demo.poc.repository.GreetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class GreetingController {
    @Autowired
    private GreetRepository greetRepository;
    @RequestMapping(value = "/greeting",method = RequestMethod.GET)
    public List<Greeting> greeting() {
        List<Greeting> greetings = new ArrayList<>();
        greetRepository.findAll().forEach(e->greetings.add(e));
        return greetings;
    }
    @RequestMapping(value = "/greeting/{user}/",method = RequestMethod.GET)
    public List<Greeting> greetingUserLimit(@PathVariable String user, Integer limit) {
        List<Greeting> greetings = new ArrayList<>();
        greetRepository.findByUser(user,limit).forEach(e -> greetings.add(e));
        return greetings;
    }
    @RequestMapping(value = "/greeting",method = RequestMethod.POST)
    public String saveGreeting(Greeting greeting) {
        greeting.setCreationDate(new Date());
        greetRepository.save(greeting);
        return "OK";
    }
}